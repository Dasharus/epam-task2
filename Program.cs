using System;
namespace TaskEpam
{
    public static class TaskEpam
    {
    
        public static int Task1(int n)
        {
            int result = 0;
            int temp;
            while(n>0)
            {
                temp = n % 10;
                if (temp % 2 != 0)
                {
                    result += temp;
                }
                n = n / 10;
            }
            return result;
        }

        public static int Task2(int n)
        {
            int count = 0;
            while (n >0)
            {
                count += n % 2;
                n = n / 2;
            }
            return count;
        }
        
        public static int Task3(int n)
        {
            int fib = 0;
            int fib1 = 1;
            int tmp;
            for (int i = 0; i < n; i++)
            {
                tmp = fib;
                fib = fib1;
                fib1 += tmp;
            }
            return fib1-1;
        }
    }
}
